# rhedcloud-aws-vpc-type2-cfn

This cloudformation template is for RHEDcloud's Type2 VPC. The RHEDcloud policies stack must be created before this type 2 VPC structures stack is created.

## Copyright

This software was created at Emory University. Please see NOTICE.txt for Emory's copyright and disclaimer notifications. This software was extended and modified by the RHEDcloud Foundation and is copyright 2019 by the RHEDcloud Foundation.

## License

This software is licensed under the Apache 2.0 license included in this distribution as LICENSE-2.0.txt