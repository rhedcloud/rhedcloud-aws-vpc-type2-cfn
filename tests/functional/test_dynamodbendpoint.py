"""
=============
test_dynamodb
=============

Verify ability to invoke AWS DynamoDB APIs.

Plan:

* Verify users can create, modify, query, and delete a DynamoDB table
* Verify users cannot access DynamoDB using HTTP
* DynamoDB is accessible within the VPC

${testcount:8}

"""

import json
import pytest

from aws_test_functions import (
    attach_policy,
    get_uuid,
)
from aws_test_functions.dynamodb import (
    create_table,
    put_item,
    delete_table,
)


@pytest.fixture(scope='module')
def dynamodb(session):
    """A shared handle to DynamoDB APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the DynamoDB APIs.

    """

    return session.client('dynamodb')


@pytest.fixture(scope='module')
def table(dynamodb):
    """A reusable DynamoDB table that gets deleted automatically.

    :param DynamoDB.Client dynamodb:
        A handle to the DynamoDB APIs.

    :returns:
        A dictionary with information about a DynamoDB table.

    """

    name = 'test_dynamodb-{}'.format(get_uuid())
    tbl = None

    try:
        tbl = create_table(name, dynamodb=dynamodb)
        yield tbl
    finally:
        if tbl is not None:
            delete_table(name, dynamodb=dynamodb)


@pytest.fixture(scope='module')
def query(table):
    """A reusable query.

    :param dict table:
        Information about the DynamoDB table used for testing.

    :returns:
        A dictionary.

    """

    return dict(
        TableName=table['TableName'],
        KeyConditionExpression='test_attr = :value',
        ExpressionAttributeValues={
            ':value': {
                'S': 'new value',
            },
        },
    )


@pytest.fixture(scope='module', autouse=True)
def dynamo_runner(session, script_runner_role, internal_runner):
    """Temporarily provide full access to DynamoDB to the internal script
    runner by way of its IAM Instance Profile.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param IAM.Role script_runner_role:
        The role assigned to the script runner instance profile.
    :param CommandRunner internal_runner:
        A function that makes it easy to run commands within an EC2 instance.

    :yields:
        ``instance_runner``

    """

    with attach_policy(script_runner_role, 'AmazonDynamoDBFullAccess'):
        yield internal_runner


def test_dynamodbendpoint_put(dynamodb, table):
    """Users should be able to put data into a dynamodb table.

    :testid: dynamodb-f-2-1, dynamodb-f-2-2

    :param callable raw_request:
        A function that will perform a raw HTTP request.
    :param str proto:
        Protocol to use (HTTP or HTTPS).
    :param dict query:
        The query to issue to DynamoDB.

    """
    res = put_item(table['TableName'], test_attr='new value', dynamodb=dynamodb)
    assert 'ConsumedCapacity' in res, 'Unexpected response: {}'.format(res)


def test_dynamodbendpoint_in_vpc(dynamo_runner, table):
    """Issue a DynamoDB query from within the Type 2 VPC to exercise the
    DynamoDB endpoint.

    :testid: dynamodb-f-3-1

    :param callable dynamo_runner:
        A function that will invoke a command in an EC2 instance.
    :param dict table:
        Information about the DynamoDB table to query.

    """

    conditions = {
        'test_attr': {
            'AttributeValueList': [{'S': 'new value'}],
            'ComparisonOperator': 'EQ',
        }
    }

    cmd = """#!/bin/bash
    mkdir -p ~/.aws/
    echo '[default]\nregion = us-east-1' > ~/.aws/config
    echo '{}' > key-conditions.json
    aws dynamodb query \
        --table-name "{}" \
        --key-conditions file://key-conditions.json
    """.format(
        json.dumps(conditions),
        table['TableName'],
    )

    # print('Running command in VPC: {}'.format(cmd))
    output = dynamo_runner(cmd, timeout=60, max_attempts=6)

    assert 'new value' in output, 'Unexpected output: {}'.format(output)

    data = json.loads(str(output))
    assert data['Count'] == 1
    assert data['Items'][0]['test_attr']['S'] == 'new value'
