'''
----------------------------------
test_dynamodbendpoint_policy_document.py
----------------------------------
This test verifies that the proper policy appears in the endpoint policy document
by comparing the deployed policy with the expected dynamodb policy.

'''

import json

from aws_test_functions import (
    aws_client,
    dict_to_filters,
)


@aws_client('ec2')
def get_dynamodbendpoint_policy_document(vpc_id, *, ec2=None):
    endpoint = ec2.describe_vpc_endpoints(Filters=dict_to_filters({
        'vpc-id': vpc_id,
        'service-name': 'com.amazonaws.us-east-1.dynamodb',
    }))

    return json.loads(endpoint['VpcEndpoints'][0]['PolicyDocument'])


def test_answer(vpc_id):
    endpoint_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": "*",
                "Action": "*",
                "Resource": "*"
            }
        ]
    }

    assert get_dynamodbendpoint_policy_document(vpc_id) == endpoint_policy
