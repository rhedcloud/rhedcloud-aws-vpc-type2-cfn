'''
----------------------------------
test_dynamodbendpoint_routetables.py
----------------------------------
Verify the dynamodb endpoint contains three RouteTableIds.  Then check
to make sure each of those Route Tables has a route for a VPC
endpoint (by matching to 'vpce-')

Returns true if endpoint contains 3 RouteTableIds and each of those
route tables has a route for a VPC endpoint.

Copied from VPC type 1 tests - RBH

'''

from aws_test_functions import aws_client, dict_to_filters


@aws_client('ec2')
def check_dynamodbendpoint_routetables(vpc_id, *, ec2=None):
    endpoint = ec2.describe_vpc_endpoints(Filters=dict_to_filters({
        'vpc-id': vpc_id,
        'service-name': 'com.amazonaws.us-east-1.dynamodb',
    }))
    routeTableIds = endpoint['VpcEndpoints'][0]['RouteTableIds']

    if len(routeTableIds) != 3:
        return False

    endpoint_id = endpoint['VpcEndpoints'][0]['VpcEndpointId']

    vpceRouteMatchIndex = 0
    for routeTableId in routeTableIds:
        routetables = ec2.describe_route_tables(RouteTableIds=[routeTableId])
        routes = routetables['RouteTables'][0]['Routes']

        for route in routes:
            if route['GatewayId'] == endpoint_id:
                vpceRouteMatchIndex += 1

    if vpceRouteMatchIndex != 3:
        return False

    return True


def test_answer(vpc_id):
    assert check_dynamodbendpoint_routetables(vpc_id)
