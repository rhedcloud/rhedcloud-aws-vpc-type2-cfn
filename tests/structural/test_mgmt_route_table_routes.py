'''
-------------------------------
test_mgmt_route_table_routes.py
-------------------------------

"Type": "structural",
"Name": "test_mgmt_route_state",
"Description": "Verify the default route (0.0.0.0/0) exists and is active for the route table.",
"Plan": "Describe the route table (need route table id) and check to see if default route is present and is active.",
"ExpectedResult": "Success"

Verify there are three actice routes in the route table and check to make sure they are the right ones.
Describe the route table (need subnet ID) and verify the routes are correct
local - desitination CIDR block matches VPC CIDR block & GatewayId=local,
default - desination CIDR Block is 0.0.0.0/0 & GatewayId matches VGW,
S3 endpoint - desitnation prefix begins with 'pl-') and active.
'''

from tests.structural.test_public_route_table_routes import check_subnet_route_table

ROUTE_TABLE_NAME = 'Management Route Table'


def test_answer(vpc_id):
    check_subnet_route_table(vpc_id, ROUTE_TABLE_NAME)
