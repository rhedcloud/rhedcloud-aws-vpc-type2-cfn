'''
----------------------------
test_vpn1_state.py
----------------------------

"Type": "structural",
"Name": "test_vpn1_state",
"Description": "Verify the VPN connection1 is "available",
"Plan": "Describe the vpn connections with the given tag name and check that state is available while ensuring its VpnGateway is attached to the VPC.",
"ExpectedResult": "Success"
'''

from aws_test_functions import aws_client


@aws_client('ec2')
def check_vpn_connection_state(vpc_id, tag_name, ec2=None):
    ft = {'Name': 'tag:Name', 'Values': [tag_name]}
    fv = {'Name': 'attachment.vpc-id', 'Values': [vpc_id]}
    for conn in ec2.describe_vpn_connections(Filters=[ft])['VpnConnections']:
        id = conn['VpnGatewayId']
        gws = ec2.describe_vpn_gateways(VpnGatewayIds=[id], Filters=[fv])['VpnGateways']
        if len(gws) <= 0:  # the vpn connection may belong to another vpc
            continue
        if conn['State'] != 'available':
            return False
        for a in gws[0]['VpcAttachments']:
            if a['State'] != 'attached':
                return False
        return True
    return False


def test_answer(vpc_id):
    assert check_vpn_connection_state(vpc_id, 'FirewallVPNConnection1')
