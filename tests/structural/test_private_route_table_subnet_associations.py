'''
-----------------------------------------------
test_private_route_table_subnet_associations.py
-----------------------------------------------

"Type": "structural",
"Name": "test_private_route_table_subnet_associations",
"Description": "Check to make sure the two subnets are associated with the route table.",
"Plan": "Describe the route table (need route-table-id) and validate the two subnets (need subnet-ids) are correct.",
"ExpectedResult": "Success"

Check to make sure two private subnets are associated with the private route table.
'''

from aws_test_functions import (
    check_route_table_subnet_associations,
)

ROUTE_TABLE_NAME = 'Private Route Table'
SUBNET_1_NAME = 'Private Subnet 1'
SUBNET_2_NAME = 'Private Subnet 2'


def test_answer(vpc_id):
    check_route_table_subnet_associations(
        vpc_id,
        ROUTE_TABLE_NAME,
        SUBNET_1_NAME,
        SUBNET_2_NAME,
    )
