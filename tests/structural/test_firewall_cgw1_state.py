'''
-----------------
test_firewall_cgw1_state.py
-----------------

"Type": "structural",
"Name": "test_firewall_cgw1_state",
"Description": "Verify the Client Gateway State is "available",
"Plan": "Describe the Client gateway and check that state is available.",
"ExpectedResult": "Success"
-RBH


'''

from aws_test_functions import aws_client, dict_to_filters


@aws_client('ec2')
def find_available_cgw_state(*, ec2=None):
    """Function to describe CGW state. If the state is 'available' then its successful."""

    cgw_dict = ec2.describe_customer_gateways(Filters=dict_to_filters({
        'tag:Name': 'Firewall1CustomerGateway',
    }))
    for cgw in cgw_dict['CustomerGateways']:
        if cgw['State'] != 'available':
            return False
    return True


def test_answer(vpc_id):
    assert find_available_cgw_state()
