'''
--------------------------
test_public_route_state.py
--------------------------

"Type": "structural",
"Name": "test_public_route_state",
"Description": "Verify the default route (0.0.0.0/0) exists and is active for the route table.",
"Plan": "Describe the route table (need route table id) and check to see if default route is present and is active.",
"ExpectedResult": "Success"

'''

from aws_test_functions import check_route_table


def test_answer(vpc_id):
    assert check_route_table(vpc_id, 'Public Route Table')
