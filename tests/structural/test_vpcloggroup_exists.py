'''
--------------------
test_vpcloggroup_exists.py
--------------------

"Type": "structural",
"Name": "test_vpcflowlogs_policy_exists",
"Description": "Verify that the RHEDcloudVpcFlowLogsPolicy exists.",
"Plan": "Look up the policy name by ARN and compare to the expected result, RHEDcloudVpcFlowLogsPolicy.",
"ExpectedResult": "Success"

This test loops through all of the VPCs of a particular type (specified as a parameter that is passed in)
and checks to make sure log group exists by querying for the log name and making sure it matches
the expected naming convention for each VPC.

'''

from aws_test_functions import get_flow_log_attr


def test_answer(vpc_id):
    group_name = get_flow_log_attr(vpc_id, 'LogGroupName')
    expected = '{}-FlowLogs'.format(vpc_id)
    assert group_name == expected, 'Log group name does not exist or does not match expected naming convention for {}'.format(vpc_id)
