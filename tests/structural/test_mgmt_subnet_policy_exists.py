'''
---------------------------------
test_mgmt_subnet_policy_exists.py
---------------------------------
               
"Type": "structural",
"Name": "test_mgmt_subnet_policy_exists",
"Description": "Verify that the ManagementSubnetPolicy exists.",
"Plan": "Look up the policy name by ARN and compare to the expected result, ManagementSubnetPolicy.",
"ExpectedResult": "Success"

This test verifies that the RHEDcloudManagementSubnetPolicy was created
by querying for it by name and verifying that its ARN is
correct.

'''

from aws_test_functions import (
    find_policy_for_stack,
    get_stack_name_from_vpc_id,
)


def test_answer(vpc_id):
    policy_name = 'RHEDcloudManagementSubnetPolicy'
    stack_name = get_stack_name_from_vpc_id(vpc_id)
    arn = find_policy_for_stack(stack_name, policy_name)

    assert arn.endswith(':policy/rhedcloud/' + stack_name + '-' + policy_name)
