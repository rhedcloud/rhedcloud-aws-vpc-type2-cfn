'''
----------------------------
test_vpn2_state.py
----------------------------

"Type": "structural",
"Name": "test_vpn2_state",
"Description": "Verify the VPN connection2 is "available",
"Plan": "Describe the vpn connections with the given tag name and check that state is available while ensuring its VpnGateway is attached to the VPC.",
"ExpectedResult": "Success"
'''

from tests.structural.test_vpn1_state import check_vpn_connection_state


def test_answer(vpc_id):
    assert check_vpn_connection_state(vpc_id, 'FirewallVPNConnection2')
