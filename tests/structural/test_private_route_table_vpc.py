'''
-------------------------------
test_private_route_table_vpc.py
-------------------------------

"Type": "structural",
"Name": "test_private_route_table_vpc",
"Description": "Verify the route table is associated with the correct VPC.",
"Plan": "Describe the route table (need route-table-id) and check to see if the VPC matches the VpcId created by this template.",
"ExpectedResult": "Success"

Verify the route table is associated with the correct VPC.
'''

from aws_test_functions import get_vpc_route_table

ROUTE_TABLE_NAME = 'Private Route Table'


def test_answer(vpc_id):
    assert get_vpc_route_table(vpc_id, ROUTE_TABLE_NAME)
