'''
-----------------------------------
test_mgmt_subnet_policy_attached.py
-----------------------------------

"Type": "structural",
"Name": "test_mgmt_subnet_policy_attached",
"Description": "Verify that the ManagementSubnetPolicy is attached to the RHEDcloudAdministratorRole.",
"Plan": "Look up the RHEDcloudAdministratorRole role by name, get a list of the attached policies, and verify that the ManagementSubnetPolicy is attached.",
"ExpectedResult": "Success"

This test verifies that the RHEDcloudManagementSubnetPolicy is attached
to RHEDcloudAdministratorRole
by verifying that the correct policy ARN is returned when
querying for attached policies.



'''

from aws_test_functions import (
    find_policy_for_stack,
    get_stack_name_from_vpc_id,
)

from tests.structural.retired_test_vpcflowlogs_policy_attached import is_policy_attached_to_role


def test_policy_attached(vpc_id):
    policy_name = 'RHEDcloudManagementSubnetPolicy'
    role_name = 'RHEDcloudAdministratorRole'
    stack_name = get_stack_name_from_vpc_id(vpc_id)
    arn = find_policy_for_stack(stack_name, policy_name)

    assert is_policy_attached_to_role(role_name, arn)
