'''
----------------------------------
test_private_route_table_routes.py
----------------------------------

"Type": "structural",
"Name": "test_private_route_table_routes",
"Description": "Verify there are three active routes in the route table and check to make sure they are the right ones.",
"Plan": "Describe the route table (need subnet ID) and verify the routes are correct (local - desitination CIDR block matches VPC CIDR block & GatewayId=local, default - desination CIDR Block is 0.0.0.0/0 & GatewayId matches VGW, and S3 endpoint - desitnation prefix begins with 'pl-') and active.",
"ExpectedResult": "Success"

Verify there are three actice routes in the route table and check to make sure they are the right ones.
Describe the route table (need subnet ID) and verify the routes are correct
local - desitination CIDR block matches VPC CIDR block & GatewayId=local,
default - desination CIDR Block is 0.0.0.0/0 & GatewayId matches VGW,
S3 endpoint - desitnation prefix begins with 'pl-') and active.
'''

from tests.structural.test_public_route_table_routes import check_subnet_route_table

ROUTE_TABLE_NAME = 'Private Route Table'


def test_answer(vpc_id):
    check_subnet_route_table(vpc_id, ROUTE_TABLE_NAME)
