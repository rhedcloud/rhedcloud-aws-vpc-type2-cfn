'''
-----------------
test_firewall_cgw1_ipaddress.py
-----------------

"Type": "structural",
"Name": "test_firewall_cgw1_ipaddress",
"Description": "Verify the cgw IP address is that of Palo Firewall 1",
"Plan": "Describe the customer gateway connecting to Palo Alto Firewall 1, by using tags, and check IP address.",
"ExpectedResult": "Success"
-RBH


'''

from aws_test_functions import aws_client, dict_to_filters


@aws_client('ec2')
def find_cgw_ipaddress(*, ec2=None):
    """Function to describe CGW state. If the state is 'available' then its successful."""

    cgw_dict = ec2.describe_customer_gateways(Filters=dict_to_filters({
        'tag:Name': 'Firewall1CustomerGateway',
    }))
    for cgw in cgw_dict['CustomerGateways']:
        if cgw['IpAddress'] != '52.21.50.141':
            return False
    return True


def test_answer(vpc_id):
    assert find_cgw_ipaddress()
