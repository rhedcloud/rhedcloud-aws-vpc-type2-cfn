'''
------------------------
test_dynamodbendpoint_state.py
------------------------

Describe the vpc endpoint (need vpc-id and service-name) and verify the dynamodb state is 'Available'.

Returns true if the state is 'available'.


'''

from aws_test_functions import aws_client, dict_to_filters


@aws_client('ec2')
def check_dynamodbendpoint_state(vpc_id, *, ec2=None):
    endpoint = ec2.describe_vpc_endpoints(Filters=dict_to_filters({
        'vpc-id': vpc_id,
        'service-name': 'com.amazonaws.us-east-1.dynamodb',
    }))

    return endpoint['VpcEndpoints'][0]['State'] == 'available'


def test_answer(vpc_id):
    assert check_dynamodbendpoint_state(vpc_id)
