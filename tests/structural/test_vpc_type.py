'''
----------------
test_vpc_type.py
----------------

"Type": "structural",
"Name": "test_vpc_type",
"Description": "Check to make sure RHEDcloudVpcType tag is 2.",
"Plan": "Describe the VPC (need VPC ID) and filter on RHEDcloudVpcType tag.  Expected result should be 2.",
"ExpectedResult": "Success"

Verify all the Type2 VPCs in the account have an RHEDcloudVpcType of '2'.
Returns True if all Type2 VPCs in the account have an RHEDcloudVpcType of '2'.
'''

import boto3


def test_answer(vpc_id):
    ec2 = boto3.client('ec2')
    response = ec2.describe_vpcs(VpcIds=[vpc_id])

    for tag in response['Vpcs'][0]['Tags']:
        if tag['Key'] != 'RHEDcloudVpcType':
            continue

        assert tag['Value'] == '2'
