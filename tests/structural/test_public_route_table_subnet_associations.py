'''
----------------------------------------------
test_public_route_table_subnet_associations.py
----------------------------------------------

"Type": "structural",
"Name": "test_public_route_table_subnet_associations",
"Description": "Check to make sure the two subnets are associated with the route table.",
"Plan": "Describe the route table (need route-table-id) and validate the two subnets (need subnet-ids) are correct.",
"ExpectedResult": "Success"

Check to make sure two public subnets are associated with the public route table.
'''

from aws_test_functions import (
    check_route_table_subnet_associations,
)

ROUTE_TABLE_NAME = 'Public Route Table'
SUBNET_1_NAME = 'Public Subnet 1'
SUBNET_2_NAME = 'Public Subnet 2'


def test_answer(vpc_id):
    check_route_table_subnet_associations(
        vpc_id,
        ROUTE_TABLE_NAME,
        SUBNET_1_NAME,
        SUBNET_2_NAME,
    )
