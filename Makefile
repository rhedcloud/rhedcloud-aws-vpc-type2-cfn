MARK ?= not slowtest
KEYWORD ?= test
ARGS ?=

test:
	pytest -m "$(MARK)" -k "$(KEYWORD)" $(ARGS)

slowtest:
	$(MAKE) MARK='slowtest' test

# ----------------------------------------------------------------------------
# The following variables and targets are present to try to clean up and
# recreate the pipeline script in Bitbucket.
# ----------------------------------------------------------------------------

# These variables may be overridden by environment variables with the same name
CLOUDFORMATION_STACK_NAME ?= rhedcloud-aws-rs-account
CLOUDTRAIL_BUCKET_NAME ?= serviceforge-aws-admin-122-ct1
CLOUDTRAIL_NAME ?= $(CLOUDFORMATION_STACK_NAME)-Master
RHEDCLOUD_ACCOUNT_NAME ?= Serviceforge 122
REPO_NAME ?= rhedcloud-aws-vpc-type2-cfn

CHECK_CMD ?= stack-state.py check $(REPO_NAME) \
	./rhedcloud-aws-rs-account-cfn/rhedcloud-aws-rs-account-cfn.json \
	./rhedcloud-aws-vpc-type2-cfn.json > /dev/null 2>&1

# Download the artifacts required for the tests in this repository
get-artifacts:
	download_artifact.sh rhedcloud-aws-rs-account-cfn rhedcloud-aws-rs-account-cfn.latest.zip

# Cache the checksums of each CloudFormation template used for this repository
# so we can determine whether to rebuild the stacks at a later time.
cache-states: get-artifacts
	stack-state.py save $(REPO_NAME) \
		./rhedcloud-aws-rs-account-cfn/rhedcloud-aws-rs-account-cfn.json \
		./rhedcloud-aws-vpc-type2-cfn.json

# Retrieve necessary artifacts and run a sequence of targets to clean and
# rebuild our stacks.
rebuild: get-artifacts clean rs-account vpc-type2 cache-states script-runners

# Run a sequence of targets to clean and rebuild our stacks.
local-rebuild: clean rs-account vpc-type2 script-runners

# Utility to determine whether a stack rebuild is necessary
should-rebuild:
	bash -c "$(CHECK_CMD) && echo No rebuild necessary || echo Rebuild necessary"

# Clean up S3 buckets, runners, and Type 2 VPC if stack changes are detected
clean:
	bash -c "$(CHECK_CMD) && $(MAKE) clean-resources || $(MAKE) clean-runners clean-resources clean-bucket clean-stack"

# Remove resources left behind by previous tests
clean-resources:
	python ./bin/cleanup.py

# Remove the CloudTrail bucket
clean-bucket:
	s3_delete_bucket.py $(CLOUDTRAIL_BUCKET_NAME)

# Remove the test runner EC2 instances
clean-runners:
	rhedcloud_script_runners.py --no-move destroy || echo 'Continuing...'

# Remove the Type 2 VPC CloudFormation stacks in parallel
clean-stack:
	cfn_stack_delete.py rhedcloud-aws-vpc-type2

# Create the rs-account CloudFormation stack
rs-account:
	bash -c "$(CHECK_CMD) || cfn_stack_create.py \
		--parameter CloudTrailName=$(CLOUDTRAIL_BUCKET_NAME) \
		--compact $(CLOUDFORMATION_STACK_NAME)-cfn \
		./rhedcloud-aws-rs-account-cfn/rhedcloud-aws-rs-account-cfn.json"

# Create the Type 2 VPC CloudFormation stacks
vpc-type2:
	bash -c "$(CHECK_CMD) || cfn_stack_create.py \
		--compact rhedcloud-aws-vpc-type2 \
		./$(REPO_NAME).json"

# Create two new EC2 instances to run scripts
script-runners:
	rhedcloud_script_runners.py --no-move setup

# Upload test artifacts
upload:
	git archive --format zip --output $(REPO_NAME).latest.zip master
	cp $(REPO_NAME).json $(REPO_NAME).latest.json
	upload_artifact.sh $(REPO_NAME).latest.json $(REPO_NAME).latest.zip


.EXPORT_ALL_VARIABLES:
