from contextlib import ExitStack
import pytest

from aws_test_functions.cleanup import cleanup

from aws_test_functions import (
    attach_policy,
    create_session_for_test_user,
    new_test_user,
)


pytest_plugins = [
    'aws_test_functions.plugin.aws_script_runner',
]


@pytest.fixture(scope='session')
def user():
    """Create a default test user"""

    policies = (
        'rhedcloud/RHEDcloudAdministratorRolePolicy',
        'AdministratorAccess',
        'rhedcloud/rhedcloud-aws-vpc-type2-RHEDcloudManagementSubnetPolicy',
    )

    with ExitStack() as stack:
        u = stack.enter_context(new_test_user('test_type2_vpc'))
        stack.enter_context(attach_policy(u, *policies))

        yield u


@pytest.fixture(scope='session')
def session(user):
    """Create a default session for the test user"""

    return create_session_for_test_user(user.user_name)


@pytest.fixture(scope='module', autouse=True)
def stack():
    """Create a reusable context manager per test module.

    :yields:
        A contextlib.ExitStack.

    """

    with ExitStack() as s:
        yield s


@pytest.fixture(scope='session', autouse=True)
def cleanup_resources():
    """Automatically clean up unused resources before running tests."""

    cleanup()


def pytest_runtest_makereport(item, call):
    if "incremental" in item.keywords:
        if call.excinfo is not None:
            parent = item.parent
            parent._prev_failed = item


def pytest_runtest_setup(item):
    if "incremental" in item.keywords:
        prev_failed = getattr(item.parent, '_prev_failed', None)
        if prev_failed is not None:
            pytest.xfail('previous test failed ({})'.format(prev_failed.name))
